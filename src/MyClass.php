<?php declare(strict_types=1);
/*
 * This file is part of "irstea/php-skeleton".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace Irstea\MyProject;

/**
 * La classe MyClass.
 */
class MyClass
{
    public function getResponse(): int
    {
        return 42;
    }
}
