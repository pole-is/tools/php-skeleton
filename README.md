Ce projet peut servir de modèle pour tout nouveau paquet PHP.

## Usage

Il peut être utilisé directement avec composer :

    composer create-project --remove-vcs irstea/php-skeleton my-package-directory

Une fois le projet crée, il y a encore quelques étapes **indispensables** à suivre pour terminer l'initialisation, cf. [CUSTOMIZATION.md](CUSTOMIZATION.md).

## Ce qu'il contient

-   un squelette d'organisation des sources ([`src/`](src) et [`tests/`](tests)),

-   un [`composer.json`](composer.json) qui inclut les outils suivants:

    -   composer-require-checker
    -   php-cs-fixer (via irstea/php-cs-fixer-config)
    -   php-parallel-lint
    -   phpcpd
    -   phploc
    -   phpmd (via irstea/phpmd-config)
    -   phpstan (via irstea/phpstan-config)
    -   phpunit
    -   security-checker

-   une préconfiguration pour les outils suivants:

    -   [editorconfig](.editorconfg)
    -   [php-cs-fixer](.php_cs.dist)
    -   [phpmd](phpmd-ruleset.xml)
    -   [phpstan](phpstan.neon)
    -   [phpunit](phpunit.xml)

-   un [`.gitlab-ci.yml`](.gitlab-ci.yml) qui utilise les outils précédemment cité.

## Ce qu'il ne contient pas

-   Un squelette d'application Symfony :

    -   Tous les packages ne sont pas des applications Symfony.
    -   Les nouvelles versions de Symfony sortent trop rapidement pour espérer suivre.
    -   Pour les nouveaux projets Symfony, il faut utiliser `symfony/flex` de toute façon.

-   Un squelette pour docker-composer :

    -   Tous les packages n'en ont pas besoin.
    -   C'est très dépendant des applications.
