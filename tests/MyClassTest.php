<?php declare(strict_types=1);
/*
 * This file is part of "irstea/php-skeleton".
 * (c) 2019-2020 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace Irstea\MyProject\Tests;

use Irstea\MyProject\MyClass;
use PHPUnit\Framework\TestCase;

/**
 * Les tests de la classe MyClass.
 */
class MyClassTest extends TestCase
{
    public function testGetResponse(): void
    {
        $subject = new MyClass();

        self::assertEquals(42, $subject->getResponse());
    }
}
