## Remplacer `README.md`

Y décrire comment utiliser le package...

## Choisir une license

Et adapter le contenu des fichiers `LICENSE` (et, optionnellement `.docheader`).

## Editer `composer.json`

#### Editer les métadonnées

Ce sont les champs et sections suivants :

-   `name`
-   `description`
-   `type`
-   `license`.
-   `keywords`.
-   `authors`.
-   `homepage`.
-   `support`.

#### Supprimer les scripts du squelette

Ce sont les scripts `post-install-cmd`, `post-update-cmd`,
`post-create-project-cmd` et `customize-reminder`.

#### Corriger les namespaces

Ils sont dans les sections `autoload` et `autoload-dev`. Ce sont par défaut
`Irstea\MyProject` et `Irstea\MyProject\Tests`, ce qui a peu de chance de
correspondre au nouveau projet.

#### Fixer les contraintes de version

Le projet inclut `irstea/dev-pack` en version `@stable` par défaut. Ce paquet va
lui-même inclure par transitivité les versions `@stable` des paquets et
configurations que nous utilisons par défaut.

Toutefois, nous allons écrire du code qui utilise
certains de ces paquets (notammment PHPUnit) et qui ne sera pas forcément
compatible avec toutes les versions. Dans ce cas, il faut préciser explicitement
les versions des paquets utilisés. Normalement, la job "php:composer-require-checker"
devrait vous indiquer quand vous faites référence à une librairie qui n'est pas
explicitement référencées.

_Nota bene_: `roave/security-advisories` doit toujours être sur `dev-master`.
